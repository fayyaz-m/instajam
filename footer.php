		</main><!-- /#main -->
		<footer id="footer" class="mt-3">
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<picture>
							<a class="mx-auto" href="<?php echo esc_url( home_url() ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
								<img src="<?php echo esc_url( home_url() . '/wp-content/themes/instajam/assets/img/JMSTR_logo-mark.png' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" />
							</a>
						</picture>
					</div>
					<div class="clearfix"></div>
					<div class="col-12 text-center">
						<?php printf( esc_html__( '&copy; %1$s %2$s', 'instajam' ), date_i18n( 'Y' ), get_bloginfo( 'name', 'display' ) ); ?>
					</div>
				</div>
			</div><!-- /.container -->
		</footer><!-- /#footer -->
	</div><!-- /#wrapper -->
	<?php
		wp_footer();
	?>
</body>
</html>
