<?php
/**
 * The template for displaying content in the index.php template.
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-4 col-xxl-4' ); ?>>
	<div class="card mt-3 mb-3">
		<div class="card-body">
			<div class="card-text entry-content">
				<?php
					if ( has_post_thumbnail() ) :
						echo '<div class="post-thumbnail">' . get_the_post_thumbnail( get_the_ID()) . '</div>';
					endif;
				?>
				<?php wp_link_pages( array( 'before' => '<div class="page-link"><span>' . esc_html__( 'Pages:', 'instajam' ) . '</span>', 'after' => '</div>' ) ); ?>
			</div><!-- /.card-text -->
			<tags class="entry-meta">
				<?php
					$post_tags = get_the_tags();
					if ( ! empty( $post_tags ) ) {
						foreach( $post_tags as $post_tag ) {
							echo '<a href="' . get_tag_link( $post_tag ) . '" class="card-link fw-bold fst-italic link-light">#' . $post_tag->name . '</a>';
						}
					}
				?>
			</tags><!-- /.entry-meta -->
		</div><!-- /.card-body -->
	</div><!-- /.col -->
</article><!-- /#post-<?php the_ID(); ?> -->
