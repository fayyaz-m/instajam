## InstaJam
* Contributors: Fayyaz Khattak
* Tags: light, dark, responsive-layout, featured-images, flexible-header, microformats, post-formats, rtl-language-support, theme-options, translation-ready, accessibility-ready
* Stable tag: 3.0.3
* Tested up to: 5.7
* License: GPLv2 or later
* License URI: https://www.gnu.org/licenses/gpl-2.0.html

## What's included?
* WordPress Theme
* Bootstrap Framework
* Sass Source files
* gulp + webpack configuration
* NPM configuration to keep the required build libraries updated and to add new libraries

## Task Automation (INCASE YOU WANT TO MAKE EDITS ELSE LEAVE THIS SECTION)
This Theme comes with a built in gulp/webpack task automation. Sass files will be compiled if changed, vendor prefixes will be added automatically and the CSS will be minified. JS source files will be bundled and minified.

* Prerequisites: [Node.js](https://nodejs.org) (NPM) needs to be installed on your system
* Open the **Project directory** `/` in Terminal and install the required Node.js dependencies: gulp, webpack, Sass-Compiler, Autoprefixer, etc.
* `$ npm install`
* Run the **`watch`** script
* `$ npm run watch`
* Modify `/assets/main.scss` and `/assets/main.js`

## Technology
* [Bootstrap](https://github.com/twbs/bootstrap), [MIT license](https://github.com/twbs/bootstrap/blob/master/LICENSE)
* [Sass](https://github.com/sass/sass), [MIT license](https://github.com/sass/sass/blob/stable/MIT-LICENSE)
* [gulp](https://github.com/gulpjs/gulp), [MIT license](https://github.com/gulpjs/gulp/blob/master/LICENSE)
* [webpack](https://github.com/webpack/webpack), [MIT license](https://github.com/webpack/webpack/blob/master/LICENSE)
* [wp-bootstrap-navwalker](https://github.com/twittem/wp-bootstrap-navwalker), [GPLv2+](https://github.com/twittem/wp-bootstrap-navwalker/blob/master/LICENSE.txt)

## Installation
1. Download/clone all files and folders from the repo and put it in a zip file.
2. Install a new fresh WordPress.
3. Once the installation of WP is finished, login to your admin panel.
4. In your admin panel, go to Appearance -> Themes and click the Add New button.
5. Click Upload and Choose File, then select the theme's ZIP file. Click Install Now.
6. Click Activate to use your new theme right away.

## Add Post
1. In your admin panel, go to Posts -> New Post.
2. Add any title to the post.
3. Add Featured Image to the post. This image will show on the frontend.
4. Don't forget to add Tags as well. These Tags will be showing on frontend and will be clickable as well.
5. Save the post. Refresh your homepage!